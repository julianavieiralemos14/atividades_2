//A nota final de um estudante é calculada a partir de três notas atribuídas
// respectivamente a um trabalho de laboratório, a uma avaliação semestral
// e a um exame final.

namespace exercicio_2 {
  let nota1, nota2, nota3, peso1, peso2, peso3: number;

  nota1 = 10;
  nota2 = 6;
  nota3 = 5;
  peso1 = 2;
  peso2 = 3;
  peso3 = 5;

  let media: number;

  media =
    (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

  console.log(`a media ponderada das notas é: ` + media);

  //Faça um programa que receba as três notas, calcule e mostre a média ponderada.

  if (media >= 8 && media <= 10) {
    console.log(`a media do aluno terá o conceito A` + media);
  } else if (media >= 7 && media < 8) {
    console.log(`a media do aluno tera o conceito B` + media);
  } else if (media >= 6 && media < 7) {
    console.log(`a media do aluno tera o conceito C` + media);
  } else if (media >= 5 && media < 6) {
    console.log(`a media do aluno tera o conceito D` + media);
  } else if (media >= 0 && media < 5) {
    console.log(`a media do aluno tera o consceito E` + media);
  }
}
