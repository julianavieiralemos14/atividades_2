namespace exemplos_condicoes {
    let idade: number = 10;

    if(idade >= 18)
    {//inicio do bloco
        console.log("Pode Dirigir");
    }//fim do bloco
    else
    {//inicio do bloco
        console.log("Não pode dirigir");
    }//fim do bloco
    //ternario
    idade >= 18 ? console.log("pode dirigir") : console.log("Não pode dirigir");
}

